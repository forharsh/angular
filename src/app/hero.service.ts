import { Injectable } from '@angular/core';
import { HEROES } from './heroes/mock.hero';
import { Hero } from './heroes/hero';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';

@Injectable()
export class HeroService {

  constructor() { }

  getHeroes() : Observable<Hero[]> {
  		return Observable.of(HEROES);
  }

}
