import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../heroes/hero';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

	//@Input('hero') salman: Hero;

	constructor(private route: ActivatedRoute) { }

	ngOnInit() {
			console.log(this.route.snapshot.paramMap.get('id'));
	}

}
