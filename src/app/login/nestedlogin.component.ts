import { Component } from '@angular/core';
import { Input } from '@angular/core';


@Component({
  selector: 'nested-app-login',
  templateUrl: './nestedlogin.component.html'
})
export class NestedLoginComponent {
  title = 'NestedLoginComponent';
  story: String = "My Story";
  @Input() secondcomponent:String;
}
