import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'story-vehicles-root',
  template: `
    <router-outlet></router-outlet>
  `
})
export class VehiclesComponent { }


/*
Copyright 2016 JohnPapa.net, LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://bit.ly/l1cense
*/